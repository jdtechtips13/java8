package com.jdtechtips.lesson001;

/**
 * Sample class to sort
 * @author JDTechTips
 *
 */
public class Person {
	public int id;
	public String firstName;
	
	public Person(int id, String firstName) {
		this.id = id;
		this.firstName = firstName;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
}