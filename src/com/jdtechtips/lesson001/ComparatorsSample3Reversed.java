package com.jdtechtips.lesson001;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

/**
 * Java 8 way of reversed sorting
 * @author JDTechTips
 *
 */
public class ComparatorsSample3Reversed {
	public static void main(String[] args) {
		ComparatorsSample3Reversed sample = new ComparatorsSample3Reversed();
		
		List<Person> persons = new ArrayList<>();
		persons.add(new Person(1, "Viper"));
		persons.add(new Person(2, "Alche"));
		persons.add(new Person(3, "Sven"));
		persons.add(new Person(4, "Doom"));
		persons.add(new Person(5, "Huskar"));
		
		sample.objectSort(persons);
		sample.displayPersons(persons);
	}
	
	/**
	 * The actual method to sort in reverse order
	 * 
	 * @param persons
	 */
	public void objectSort(List<Person> persons) {
		Comparator<Person> c = (Person o1, Person o2) -> o1.getFirstName().compareTo(o2.getFirstName());
		persons.sort(c.reversed());
	}
	
	/**
	 * Method to display the list
	 * @param persons
	 */
	private void displayPersons(List<Person> persons) {
		for(Person person : persons) {
			System.out.print(person.getFirstName() + " ");
		}
		System.out.println();
	}
}


