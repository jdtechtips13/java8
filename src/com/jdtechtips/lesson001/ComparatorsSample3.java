package com.jdtechtips.lesson001;
import java.util.ArrayList;
import java.util.List;

/**
 * Java 8 way of sorting
 * @author JDTechTips
 *
 */
public class ComparatorsSample3 {
	public static void main(String[] args) {
		ComparatorsSample3 sample = new ComparatorsSample3();
		
		List<Person> persons = new ArrayList<>();
		persons.add(new Person(1, "Viper"));
		persons.add(new Person(2, "Alche"));
		persons.add(new Person(3, "Sven"));
		persons.add(new Person(4, "Doom"));
		persons.add(new Person(5, "Huskar"));
		
		sample.objectSort(persons);
		sample.displayPersons(persons);
	}
	
	/**
	 * The actual method to sort
	 * 
	 * @param persons
	 */
	public void objectSort(List<Person> persons) {
		persons.sort((Person o1, Person o2) -> o1.getFirstName().compareTo(o2.getFirstName()));
	}
	
	/**
	 * Method to display the list
	 * @param persons
	 */
	private void displayPersons(List<Person> persons) {
		for(Person person : persons) {
			System.out.print(person.getFirstName() + " ");
		}
		System.out.println();
	}
}


