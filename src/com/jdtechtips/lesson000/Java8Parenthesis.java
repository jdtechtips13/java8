package com.jdtechtips.lesson000;

public class Java8Parenthesis {
	interface HelloService {
		public void sayHello(String name);
	}

	public static void main(String[] args) {
		
		HelloService svc;
		
		// With Parenthesis
		svc = (name) -> System.out.println(name);
		
		// Without Parenthesis
		svc = name -> System.out.println(name);

		svc.sayHello("JayDee Test");
	}
}
