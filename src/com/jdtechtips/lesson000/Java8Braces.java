package com.jdtechtips.lesson000;

public class Java8Braces {
	interface HelloService {
		public void sayHello(String name);
	}

	public static void main(String[] args) {

		HelloService svc;

		// With Braces
		svc = (name) -> System.out.println(name);

		// Without Braces
		svc = name -> {
			System.out.println(name);
			System.out.println(name.length());
		};

		svc.sayHello("JayDee Test");
	}
}
