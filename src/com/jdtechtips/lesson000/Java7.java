package com.jdtechtips.lesson000;

public class Java7 {
	interface HelloService{
		public void sayHello(String name);
	}
	
	static class HelloServiceImpl implements HelloService {	
		@Override
		public void sayHello(String name) {
			System.out.println(name);
		}
	}
	
	public static void main(String[] args) {
		HelloService svc = new HelloServiceImpl();
		svc.sayHello("TechTips");
	}
}

