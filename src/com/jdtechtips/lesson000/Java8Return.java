package com.jdtechtips.lesson000;

public class Java8Return {
	interface HelloService {
		public String sayHello(String name);
	}

	public static void main(String[] args) {

		HelloService svc;

		// Without Braces
		svc = (name) -> name.toUpperCase();

		// With Braces
		svc = name -> {
			return name.toUpperCase();
		};

		System.out.println(svc.sayHello("TechyTipsy"));
	}
}
