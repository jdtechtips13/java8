package com.jdtechtips.lesson000;

public class Java8Type {
	interface HelloService {
		public void sayHello(String name);
	}

	public static void main(String[] args) {
		
		HelloService svc;
		
		// With Type
		svc = (String name) -> System.out.println(name);
		
		// Without Type
		svc = (name) -> System.out.println(name);

		svc.sayHello("JayDee");
	}
}
